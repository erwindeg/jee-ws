package nl.sogeti.demo.common;

import java.util.Properties;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * This enumeration takes care of the dynamic lookup (no restart required) of properties
 * Properties are defined as KEY(defaultvalue) enumerations. 
 * Upon retrieval the property resource is loaded, if a property with KEY exists, the corresponding value is returned, else the defaultvalue is returned
 * 
 * @author giererwi
 *
 */
public enum Parameters {
	TEST("default"),
	ENDPOINT_WS("http://localhost:8080/");
	
	private static final String PROPERTY_FILENAME = "demo-properties";
	
	//Injecting into an enum is not yet possible with CDI
	Logger logger = Logger.getLogger(Parameters.class.getName());

	private String defaultValue;

	private Parameters(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	/**
	 * @return the value for this enum if exists, otherwise the default
	 */
	public String getValue() {
		Properties properties = loadProperties();
		if (properties.containsKey(this.toString())) {
			return properties.getProperty(this.toString());
		} else {
			return this.defaultValue;
		}
	}

	private Properties loadProperties() {
		Context initialContext;
		Properties properties = new Properties();;
		try {
			initialContext = new InitialContext();
			properties = (Properties) initialContext.lookup(PROPERTY_FILENAME);
		} catch (NamingException e) {
			logger.info("Lookup of demo-properties failed, using defaults: "+e.getMessage());
		}
		
		return properties;
	}

}
