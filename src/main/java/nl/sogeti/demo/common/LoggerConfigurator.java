/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.sogeti.demo.common;

import java.util.logging.Logger;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

/**
 * @author giererwi
 */
public class LoggerConfigurator {

	@Produces
	public Logger get(InjectionPoint ip) {
		Class<?> requestingClass = ip.getMember().getDeclaringClass();
		return Logger.getLogger(requestingClass.getName());
	}

}
