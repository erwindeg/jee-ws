package nl.sogeti.demo.controller;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;

import nl.sogeti.demo.common.Parameters;
import nl.sogeti.demo.model.Person;
import nl.sogeti.demo.service.PersonService;

@ManagedBean
@SessionScoped
public class PersonBean implements Serializable {
 
	private static final long serialVersionUID = 1L;
	
	@Inject
	Logger logger;
	
	@Inject
	PersonService personService;
 
	private String name;
 
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public List<Person> list(){
		return personService.findAll();
	}
	
	public String getTestParameter(){
		return Parameters.TEST.getValue();
	}
	
	public String save(){
		if(name != null){
			Person person = new Person();
			person.setName(this.name);
			personService.save(person);
			logger.info("Person saved");
		}
		
		return "hello";
	}
}