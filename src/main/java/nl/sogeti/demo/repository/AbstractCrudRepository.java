/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.sogeti.demo.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 * @author giererwi
 * @param <T> The entity to use the CRUD on.
 */

public abstract class AbstractCrudRepository<T>
{
   @PersistenceContext
   EntityManager entityManager;
   
   protected abstract Class<T> getEntityClass();
   
   public T persist(T entity)
   {
      this.entityManager.persist(entity);
      return entity;
   }
   
   public T merge(T entity)
   {
      return this.entityManager.merge(entity);
   }
   
   public void remove(T entity)
   {
      T attached = this.entityManager.merge(entity);
      this.entityManager.remove(attached);
   }   
   
   public T find(Long key)
   {
      return this.entityManager.find(getEntityClass(), key);
   }
   
   public List<T> findAll() {
		TypedQuery<T> query = entityManager.createQuery("SELECT e FROM "
				+ getEntityClass().getSimpleName() + " e", getEntityClass());
		return query.getResultList();

	}   
   

   
}
