package nl.sogeti.demo.repository;

import javax.ejb.Stateless;

import nl.sogeti.demo.model.Person;

@Stateless
public class PersonRepository extends AbstractCrudRepository<Person>{

	@Override
	protected Class<Person> getEntityClass() {
		return Person.class;
	}

}
