package nl.sogeti.demo.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;

import nl.sogeti.demo.model.Person;
import nl.sogeti.demo.repository.PersonRepository;

/**
 * Stateless EJB exposed as a WebService on location /PersonServiceService/PersonService?wsdl
 * 
 * @author Erwin
 *
 */
@WebService
@Stateless
@LocalBean
public class PersonService {

	@Inject
	PersonRepository personRepository;

	@WebMethod
	public Person save(Person person) {
		return this.personRepository.persist(person);
	}

	@WebMethod
	public List<Person> findAll() {
		return personRepository.findAll();
	}
}
