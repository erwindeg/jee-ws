package nl.sogeti.demo.service;

import java.util.List;

import javax.ejb.Local;

import nl.sogeti.demo.model.Person;

/**
 * Use a local interface, because otherwise the EJB with @WebService annotation cannot be injected.
 * 
 * @author Erwin
 *
 */
@Local
public interface PersonServiceLocal {
	public Person save(Person person);
	public List<Person> findAll();
}
