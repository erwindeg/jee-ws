package nl.sogeti.demo.repository;

import static org.junit.Assert.assertNotNull;
import nl.sogeti.demo.model.Person;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PersonRepositoryTest extends AbstractTest {

	PersonRepository repository;

	@Before
	public void setup() {
		repository = new PersonRepository();
		repository.entityManager = getEntityManager();
		getTransaction().begin();
	}

	@Test
	public void testPersist() {
		
		Person person = new Person();
		person.setName("test");
		person = repository.persist(person);
		assertNotNull(repository.find(person.getId()));
	}

	@After
	public void teardown() {
		if (getTransaction().isActive()) {
			getTransaction().rollback();
		}
	}

}
